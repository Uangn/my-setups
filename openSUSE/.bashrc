# aliases
alias v=nvim
alias e="emacs -nw"

alias poweroff="systemctl poweroff"
alias reboot="systemctl reboot"

alias cs="xclip -selection clipboard"

sa() {
    if [[ $# -eq 1 ]]; then
        ssh-add "$HOME/.ssh/$1"
    else
        echo "invalid parameter count ($#)"
    fi
}

make-watch() {
    makecmd=$1;
    shift;
    while inotifywait -qqre modify,create,delete,move $@;
    do
        $makecmd;
    done
}

tmux-n() {
    if [ -z "$TMUX" ]; then
        tmux new -s $(tmux-new-name) $@;
    else
        tmux new -d -s $(tmux-new-name) $@;
        tmux switchc -t $(tmux-new-name) $@;
    fi
}

tmux-a() {
    if [ -z "$TMUX" ]; then
        tmux a $@;
    else
        tmux-n $@;
    fi
}

tmux-new-name() {
    GIT_ROOT="$(git rev-parse --show-toplevel 2>/dev/null)"
    GIT="$(git rev-parse --show-prefix 2>/dev/null)"
    if [ $? -ne 0 ]; then
        GIT="${PWD##*/}"
    elif [ -n "$GIT" ]; then
        GIT="${GIT_ROOT##*/}/${GIT%/}"
    else
        GIT="${GIT_ROOT##*/}"
    fi
    echo "$GIT"
}


# starship
eval "$(starship init bash)"

# zoxide
eval "$(zoxide init bash)"

# brew
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# asdf
export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"

# ssh
# export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
# eval "$(ssh-agent)" &> /dev/null
# if [ $? -ge 2 ]; then
#     ssh-agent -a "$SSH_AUTH_SOCK" > /dev/null
# fi
if [ -z "$SSH_AUTH_SOCK" ]; then
    eval "$(ssh-agent)" &> /dev/null
fi

alias v=nvim
alias e="emacs -nw"

alias poweroff="systemctl poweroff"
alias reboot="systemctl reboot"

alias cs="xclip -selection clipboard"

retry-cmd() {
    local n=0
    local try=$1
    local cmd="${@: 2}"
    [[ $# -le 1 ]] && echo "Usage $0 <retry_number> <Command>"
    [[ $try -le 0 ]] && { try=99999; }

    until [[ $n -ge $try ]]
    do
        $cmd && break || {
            echo ">>> Command Failed"
            ((n++))
            echo ">>> retry $n ::"
            sleep 1;
        }
    done
}

sa() {
    if [[ $# -eq 1 ]]; then
        ssh-add "$HOME/.ssh/$1"
    else
        echo "invalid parameter count ($#)"
    fi
}

make-watch() {
    makecmd=$1;
    shift;
    while inotifywait -qqre modify,create,delete,move $@;
    do
        $makecmd;
    done
}

tmux-n() {
    if [ -z "$TMUX" ]; then
        tmux new -s $(tmux-new-name) $@;
    else
        tmux new -d -s $(tmux-new-name) $@;
        tmux switchc -t $(tmux-new-name) $@;
    fi
}

tmux-a() {
    if [ -z "$TMUX" ]; then
        tmux a $@;
    else
        tmux-n $@;
    fi
}

tmux-new-name() {
    GIT_ROOT="$(git rev-parse --show-toplevel 2>/dev/null)"
    GIT="$(git rev-parse --show-prefix 2>/dev/null)"
    if [ $? -ne 0 ]; then
        GIT="${PWD##*/}"
    elif [ -n "$GIT" ]; then
        GIT="${GIT_ROOT##*/}/${GIT%/}"
    else
        GIT="${GIT_ROOT##*/}"
    fi
    echo "$GIT"
}

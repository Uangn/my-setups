{- the kinda minimal config
 - Checkout: `hoogle.haskell.org`, `hackage.haskell.org`
 -}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}

import XMonad (Default (..), (-->), (<+>), (=?))
import qualified XMonad as XM
import qualified XMonad.Actions.EasyMotion as EasyMotion
import qualified XMonad.Actions.FocusNth as Actions
import qualified XMonad.Actions.Submap as Submap
import qualified XMonad.Actions.UpdatePointer as Actions
import qualified XMonad.Hooks.DynamicLog as Hooks
import qualified XMonad.Hooks.EwmhDesktops as Hooks
import qualified XMonad.Hooks.ManageDocks as Hooks
import qualified XMonad.Hooks.ManageHelpers as Hooks
import qualified XMonad.Hooks.StatusBar as Hooks
import qualified XMonad.Hooks.UrgencyHook as Hooks
import XMonad.Layout ((|||))
import qualified XMonad.Layout as Layout
import qualified XMonad.Layout.AvoidFloats as Layout
import qualified XMonad.Layout.IndependentScreens as Layout
import qualified XMonad.Layout.LayoutModifier as Layout
import qualified XMonad.Layout.MultiToggle as MultiToggle
import qualified XMonad.Layout.MultiToggle.Instances as MultiToggle
import qualified XMonad.Layout.NoBorders as Layout
import qualified XMonad.Layout.TwoPanePersistent as Layout
import qualified XMonad.StackSet as StackSet
import qualified XMonad.Util.EZConfig as Util
import qualified XMonad.Util.Hacks as Util
import qualified XMonad.Util.SpawnOnce as Util
import qualified XMonad.Util.XUtils as Util

import qualified Control.Monad as Monad
import qualified Control.Monad.State as State
import Data.Bits ((.|.))
import qualified Data.Foldable as Foldable
import qualified Data.List as List
import qualified Data.Map as Map
import Graphics.X11.Xlib
import qualified System.Directory as System
import qualified System.Exit as System

main :: IO ()
main = do
    nScreens <- (Layout.countScreens :: IO Int)
    hasBattery <- System.doesFileExist "/sys/class/power_supply/BAT0/present"
    let sbs = zipWith (statusBar hasBattery) [0 ..] ["xmobarrc", "xmobarrc"]

    XM.xmonad
        . compose' (Hooks.withSB <$> take (fromIntegral nScreens) sbs)
        . Util.javaHack
        . Hooks.setEwmhActivateHook Hooks.doAskUrgent
        . Hooks.ewmh
        . Hooks.docks
        $ def
            { -- style
              XM.borderWidth = 4
            , XM.normalBorderColor = normalBorderColor
            , XM.focusedBorderColor = focusedBorderColor
            , -- Startup
              XM.startupHook = do
                setToggleActiveAll MTAvoidStruts True
                Actions.updatePointer (0.5, 0.5) (1, 1)
                Util.spawnOnce "source $HOME/.xprofile"
            , -- Layout
              XM.layoutHook =
                Layout.lessBorders Layout.OnlyFloat
                    . Layout.avoidFloats
                    . MultiToggle.mkToggle1 MTEmpty
                    . MultiToggle.mkToggle1 MTAvoidStruts
                    . MultiToggle.mkToggle1 MultiToggle.NBFULL
                    $ Layout.Tall 1 (3 / 100) (1 / 2)
                        ||| Layout.TwoPanePersistent Nothing (3 / 100) (1 / 2)
            , -- Log / Output
              XM.logHook = pure ()
            , -- Manage Window
              XM.manageHook =
                XM.composeAll
                    [ Hooks.isFullscreen --> (XM.doF StackSet.focusDown <+> Hooks.doFullFloat)
                    , XM.className =? "OpenTabletDriver.UX" --> XM.doFloat
                    , -- , className =? "Gimp"                --> XM.doFloat
                      XM.className =? "xdg-desktop-portal-gtk" --> XM.doFloat
                    , XM.className =? "steam" --> tryMoveTo ["ent"]
                    , XM.resource =? "desktop_window" --> XM.doIgnore
                    , XM.resource =? "kdesktop" --> XM.doIgnore >> tryMoveTo ["games", "media"]
                    , XM.className =? "discord" --> tryMoveTo ["social"]
                    , XM.className =? "vesktop" --> tryMoveTo ["social"]
                    , XM.className =? "VencordDesktop" --> tryMoveTo ["social"]
                    , XM.className =? "Spotify" --> tryMoveTo ["music"]
                    , XM.className =? "obs" --> tryMoveTo ["rec", "media"]
                    , XM.className =? "krita" --> tryMoveTo ["draw", "media"]
                    ]
            , -- Events
              XM.handleEventHook =
                XM.handleEventHook def
                    <> Util.fixSteamFlicker
                    <> Util.windowedFullscreenFixEventHook
            , -- Settings
              XM.focusFollowsMouse = False
            , XM.clickJustFocuses = False
            , XM.terminal = "alacritty"
            , XM.workspaces = ["home", "work", "search", "dev", "temp", "music", "ent", "config", "social", "media", "notes", "test"]
            , -- Keys
              XM.modMask = modm1
            , XM.keys = \conf@(XM.XConfig{XM.terminal}) ->
                Map.fromList $
                    [ -- Launch / Close Windows
                      ((modm1, xK_p), XM.spawn (rofi <> "-show run"))
                    , ((modm1 .|. modm2, xK_p), XM.spawn (rofi <> "-show window"))
                    , ((modm1 .|. modm2, xK_q), XM.kill)
                    , ((modm1, xK_Return), XM.spawn terminal)
                    , -- Focus / Swap Windows
                      ((modm1, xK_j), XM.windows StackSet.focusDown)
                    , ((modm1 .|. modm2, xK_j), XM.windows StackSet.swapDown)
                    , ((modm1, xK_k), XM.windows StackSet.focusUp)
                    , ((modm1 .|. modm2, xK_k), XM.windows StackSet.swapUp)
                    , ((modm1, xK_m), XM.windows StackSet.focusMaster)
                    , ((modm1 .|. modm2, xK_m), XM.windows StackSet.swapMaster)
                    , ((modm1, xK_semicolon), mySelectWindow >>= (`XM.whenJust` XM.windows . StackSet.focusWindow))
                    ,
                        ( (modm1 .|. modm2, xK_semicolon)
                        , do
                            win <- mySelectWindow
                            stack <- State.gets (StackSet.index . XM.windowset)
                            let match = List.find ((win ==) . Just . fst) (zip stack [0 ..])
                            XM.whenJust match (Actions.swapNth . snd)
                        )
                    , -- Change Size
                      ((modm1, xK_h), XM.sendMessage XM.Shrink)
                    , ((modm1, xK_l), XM.sendMessage XM.Expand)
                    , ((modm1, xK_comma), XM.sendMessage (XM.IncMasterN 1))
                    , ((modm1, xK_period), XM.sendMessage (XM.IncMasterN (-1)))
                    , -- Options

                        ( (modm1, xK_o)
                        , Submap.visualSubmapSorted (List.sortOn snd) windowConfig . Map.fromList $
                            [ ((0, xK_t), ("Toggle Float", XM.withFocused toggleFloat))
                            , ((0, xK_e), ("Toggle Empty", XM.sendMessage (MultiToggle.Toggle MTEmpty)))
                            , ((0, xK_f), ("Toggle NoBorder Fullscreen", XM.sendMessage (MultiToggle.Toggle MultiToggle.NBFULL)))
                            , ((0, xK_d), ("Toggle NoBorder Fullscreen + AvoidStruts", mapM_ XM.sendMessage [MultiToggle.Toggle MultiToggle.NBFULL, MultiToggle.Toggle MTAvoidStruts]))
                            , ((0, xK_b), ("Toggle AvoidStruts", XM.sendMessage (MultiToggle.Toggle MTAvoidStruts)))
                            , ((0, xK_n), ("Layout Next", XM.sendMessage XM.NextLayout))
                            , ((modm1, xK_space), ("Layout Default", XM.setLayout (XM.layoutHook conf) *> layoutDefaults))
                            ]
                        )
                    , -- IO

                        ( (modm1, xK_i)
                        , Submap.visualSubmap windowConfig . Map.fromList $
                            [ ((0, xK_s), ("Sleep", XM.spawn lockscreenCmd))
                            , ((0, xK_r), ("Reload Config", XM.spawn "killall xmobar ; xmonad --recompile && xmonad --restart"))
                            , ((modm1 .|. modm2, xK_q), ("Exit XMonad", XM.io System.exitSuccess))
                            ]
                        )
                    ]
                        -- Focus / Swap Workspaces
                        <> [ ((modm1 .|. m, k), XM.windows (f i))
                           | (i, k) <- zip (XM.workspaces conf) workspaceKeys
                           , (f, m) <- [(StackSet.greedyView, 0), (StackSet.shift, modm2)]
                           ]
                        -- Focus / Swap Screens
                        <> [ ((modm1 .|. m, k), XM.screenWorkspace sc >>= flip XM.whenJust (XM.windows . f))
                           | (k, sc) <- zip screenKeys [0 ..]
                           , (f, m) <- [(StackSet.view, 0), (StackSet.shift, modm2)]
                           ]
            , -- Mouse
              XM.mouseBindings = \conf ->
                Map.fromList
                    [ ((modm1, button1), \w -> XM.focus w *> XM.mouseMoveWindow w *> XM.windows StackSet.shiftMaster)
                    , ((modm1, button2), \w -> XM.focus w *> XM.windows StackSet.shiftMaster)
                    , ((modm1, button3), \w -> XM.focus w *> XM.mouseResizeWindow w *> XM.windows StackSet.shiftMaster)
                    ]
            }
            `Util.additionalKeysP` additionalKeys

-- Additional Keys
additionalKeys :: [(String, XM.X ())]
additionalKeys =
    [ -- Audio
      ("<XF86AudioLowerVolume>", XM.spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
    , ("<XF86AudioRaiseVolume>", XM.spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
    , ("C-<XF86AudioLowerVolume>", XM.spawn "pactl set-sink-volume @DEFAULT_SINK@ -1%")
    , ("C-<XF86AudioRaiseVolume>", XM.spawn "pactl set-sink-volume @DEFAULT_SINK@ +1%")
    , ("<XF86AudioMute>", XM.spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    , ("<XF86AudioMicMute>", XM.spawn "$HOME/.local/bin/scripts/mute-microphone.sh")
    , ("M-\\", XM.spawn "$HOME/.local/bin/scripts/mute-microphone.sh")
    , -- Brightness
      ("<XF86MonBrightnessUp>", XM.spawn "brightnessctl set +5%")
    , ("<XF86MonBrightnessDown>", XM.spawn "brightnessctl --min-value=1% set 5%-")
    , ("C-<XF86MonBrightnessUp>", XM.spawn "brightnessctl set +1%")
    , ("C-<XF86MonBrightnessDown>", XM.spawn "brightnessctl --min-value=1% set 1%-")
    , -- Screenshots
      ("<Print>", XM.spawn ("flameshot full --path " <> screenshotMain))
    , ("S-<Print>", XM.spawn ("flameshot gui --path " <> screenshotMain))
    , ("C-<Print>", XM.spawn "flameshot full --clipboard")
    , ("C-S-<Print>", XM.spawn "flameshot gui --clipboard")
    ]

-- Layout
toggleFloat :: Window -> XM.X ()
toggleFloat w =
    XM.windows $ \s ->
        if Map.member w (StackSet.floating s)
            then StackSet.sink w s
            else StackSet.float w (StackSet.RationalRect (1 / 3) (1 / 4) (1 / 2) (4 / 5)) s

unmodifyLayout :: Layout.ModifiedLayout m l a -> l a
unmodifyLayout (Layout.ModifiedLayout _ l) = l

data MyToggles
    = MTEmpty
    | MTAvoidStruts
    deriving (Read, Show, Eq, XM.Typeable)

instance MultiToggle.Transformer MyToggles Window where
    transform MTEmpty x k = k EmptyLayout (const x)
    transform MTAvoidStruts x k = k (Hooks.avoidStruts x) unmodifyLayout

data EmptyLayout a = EmptyLayout
    deriving (Show, Read)
instance XM.LayoutClass EmptyLayout a where
    doLayout a b _ = XM.emptyLayout a b
    description _ = "Empty"

layoutDefaults :: XM.X ()
layoutDefaults = do
    XM.sendMessage (MultiToggle.Toggle MTAvoidStruts)

-- Utils
infixl 4 <<$>>
(<<$>>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<<$>>) = fmap . fmap

compose' :: [a -> a] -> a -> a
compose' = Foldable.foldl' (flip (.)) id

statusBar :: Bool -> Int -> String -> Hooks.StatusBarConfig
statusBar hasBattery n name =
    Hooks.statusBarProp
        (unwords [xmobarCommand (modifierName name), "-x", show n])
        (pure myDefPP)
  where
    modifierName s = if hasBattery then "battery-" <> s else s

tryMoveTo :: [String] -> XM.ManageHook
tryMoveTo [] = def
tryMoveTo (w : ws) =
    mfindWorkspace w >>= maybe (tryMoveTo ws) XM.doShift

mfindWorkspace :: String -> XM.Query (Maybe XM.WorkspaceId)
mfindWorkspace w =
    XM.liftX
        ( XM.withWindowSet $
            pure
                . List.find (w ==)
                . fmap StackSet.tag
                . StackSet.workspaces
        )

mapWorkspaces :: (XM.WindowSpace -> XM.X b) -> XM.X [b]
mapWorkspaces f = XM.withWindowSet (traverse f . StackSet.workspaces)

setToggleActiveAll :: (MultiToggle.Transformer t Window) => t -> Bool -> XM.X ()
setToggleActiveAll t a = Monad.void (mapWorkspaces (setToggleActive t a))

setToggleActive :: (MultiToggle.Transformer t Window) => t -> Bool -> XM.WindowSpace -> XM.X ()
setToggleActive t b ws = do
    g <- (if b then not else id) <<$>> MultiToggle.isToggleActive t ws
    maybe (pure ()) (`Monad.when` XM.sendMessageWithNoRefresh (MultiToggle.Toggle t) ws) g

-- Settings
lockscreenCmd :: String
-- lockscreenCmd = "xset s activate"
lockscreenCmd = "xautolock -locknow"

rofi :: String
rofi = "rofi -monitor 1 -modes 'run,window' "

modm1, modm2, modm3, modm4 :: KeyMask
modm1 = XM.mod4Mask
modm2 = XM.shiftMask
modm3 = XM.controlMask
modm4 = XM.mod1Mask

workspaceKeys :: [KeySym]
workspaceKeys = [xK_a, xK_s, xK_d, xK_f, xK_g, xK_z, xK_x, xK_c, xK_v, xK_b] <> [xK_1 .. xK_9] <> [xK_0]

screenKeys :: [KeySym]
screenKeys = [xK_e, xK_r, xK_t]

brightFgColor, fgColor, dimfgColor, dimmerfgColor, bgColor, normalBorderColor, focusedBorderColor, brightFocusedBorderColor, sepColor :: String
brightFgColor = "#b4637a"
fgColor = "#8990b3"
dimfgColor = "#575279"
dimmerfgColor = "#433E65"
bgColor = "#e1e2e7"
normalBorderColor = "#e9e9ed"
focusedBorderColor = "#907aa9"
brightFocusedBorderColor = "#DB8E25"
sepColor = "#444444"

screenshotMain :: String
screenshotMain = show "$HOME/Pictures/screenshots/"

emConf :: EasyMotion.EasyMotionConfig
emConf =
    def
        { EasyMotion.emFont = "xft: FiraCode-30"
        , EasyMotion.bgCol = bgColor
        , EasyMotion.txtCol = fgColor
        , EasyMotion.borderCol = focusedBorderColor
        , EasyMotion.borderPx = 10
        , EasyMotion.sKeys = EasyMotion.AnyKeys [xK_j, xK_k, xK_h, xK_l]
        , EasyMotion.overlayF = EasyMotion.proportional (0.7 :: Double)
        , EasyMotion.cancelKey = xK_Escape
        }

mySelectWindow :: XM.X (Maybe Window)
mySelectWindow = EasyMotion.selectWindow emConf

-- WindowConfig
windowConfig :: Util.WindowConfig
windowConfig =
    def
        { Util.winBg = bgColor
        , Util.winFg = fgColor
        }

-- xmobar
xmobarCommand :: String -> String
xmobarCommand name =
    unwords
        [ "xmobar"
        , "$HOME/.config/xmobar/" <> name
        , "-B" <> normalBorderColor
        , "-F" <> focusedBorderColor
        ]

myDefPP :: Hooks.PP
myDefPP =
    def
        { Hooks.ppCurrent = xmobarColorBox brightFocusedBorderColor
        , Hooks.ppVisible = xmobarColorBox brightFgColor
        , Hooks.ppHidden = xmobarColorBox focusedBorderColor
        , Hooks.ppHiddenNoWindows = xmobarColorBox dimmerfgColor
        , Hooks.ppLayout = xmobarColorBox focusedBorderColor
        , Hooks.ppTitle = xmobarColorBox focusedBorderColor . Hooks.shorten 35
        , Hooks.ppSep = concat ["<fc=", sepColor, "> <> </fc>"]
        }
  where
    xmobarColorBox c = xmobarColorWrapBox c "" ""
    xmobarColorWrapBox c wl wr = Hooks.xmobarColor c "" . Hooks.wrap wl wr . boxWrap c
    boxWrap x = Hooks.wrap ("<box type=Bottom width=1 mb=1 color=" <> x <> ">") "</box>"

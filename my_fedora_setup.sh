#!/bin/sh

ALL=$( [[ ! "$1" = "-y" ]]; echo $? )

function yn {
    if (( $ALL )); then
        return 0
    else
        while true; do
            read -p "$* [y/n]: " yn
            case $yn in
                [Yy]*) return 0;;
                [Nn]*) echo "Aborted"; return 1;;
            esac
        done
    fi
}

function throw_bashrc {
    echo "
$1
" >> ~/.bashrc
}

function sshagent {
    mkdir -p ~/.config/systemd/user
    printf '[Unit]\nDescription=SSH key agent\n\n[Service]\nType=simple\nEnvironment=SSH_AUTH_SOCK=%%t/ssh-agent.socket\nExecStart=/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK\n\n[Install]\nWantedBy=default.target' > ~/.config/systemd/user/ssh-agent.service
    throw_bashrc 'export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"'
    mkdir -p ~/.ssh/config
}
yn "sshagent" && sshagent

function dirs {
    mkdir -p ~/Downloads
}
yn "dirs?" && dirs


DOT_DIR=~/Documents/dotfiles

# My Dots
function nvim {
    git clone https://gitlab.com/Uangn/my-nvim.git ~/.config/nvim
    throw_bashrc \
"export VISUAL=nvim
export EDITOR=\"\$VISUAL\""
}
yn "Neovim Dots?" && nvim

function dots {
    mkdir -p $DOT_DIR
    git clone https://gitlab.com/Uangn/dotfiles.git $DOT_DIR
    mkdir -p ~/.config
    cp -r $DOT_DIR/.config/* ~/.config
    mkdir -p ~/.local
    cp -r $DOT_DIR/.local/* ~/.local
    mkdir -p ~/.bashrc.d
    cp -r $DOT_DIR/.bashrc.d/* ~/.bashrc.d
    mkdir -p ~/Pictures/Wallpapers
    cp -r $DOT_DIR/Pictures/Wallpapers/* ~/Pictures/Wallpapers
    cp -r $DOT_DIR/.xprofile ~/.xprofile
    cp -r $DOT_DIR/.inputrc ~/.inputrc
    cp -r $DOT_DIR/.haskeline ~/.haskeline
    cp -r $DOT_DIR/.ghci ~/.ghci
    cp -r $DOT_DIR/.Xmodmap ~/.Xmodmap
    cp -r $DOT_DIR/.gtkrc-2.0 ~/.gtkrc-2.0

    throw_bashrc "export PS1=' \u@\h \W$ 'i"
}
yn "Dotfiles?" && dots

# rpm fusion
yn "rpmfusion?" && sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm && sudo dnf config-manager --enable fedora-cisco-openh264

sudo dnf update -y

# Programs
programs=(
    # package managers
    flatpak
    # online grabbers
    git wget curl
    # online givers
    croc
    # terms (xterm for xmonad default backup)
    # xterm
    alacritty
    # cli
    btop
    unar p7zip p7zip-plugins
    trash-cli tldr tmux neovim fzf bat eza ripgrep fd-find zoxide
    # tray
    stalonetray
    # file managers
    ranger pcmanfm
    # drive space
    ncdu
    # usb
    udiskie
    # screen
    brightnessctl
    # audio
        # player
    # mpd ncmpcpp
    cmus
        # downloader
    yt-dlp
        # utils
    alsa-utils pulseaudio-utils
    pavucontrol
    easyeffects
    # image
    ImageMagick krita feh
        # screenshot
    flameshot
    # video
    ffmpeg kdenlive mpv
        # celluloid
    # pdfs
    zathura zathura-pdf-mupdf
    # games
    wine steam lutris
    # peripherals
    piper solaar openrgb
    # screen
    redshift
    # clipboard
    xclip clipit
    # program launcher
    # dmenu
    rofi
    # lock screen
    xss-lock xset i3lock
    # display manager
    # greetd tuigreet
    sddm
    # compositor
    picom
    # notifications
    dunst
    # theming
    arc-theme
    # bluetooth
    blueman
    # NetworkManager
    NetworkManager-tui
    # support (idk u might not need this)
    @"Hardware Support"
    @"Printing Support"
    # vm stuff
    @Virtualization
    # sum fonts
    @Fonts
    fontawesome-fonts
    # firewall
    ufw
    # display
    @base-x
    # xmonad
    libX11-devel libXft-devel libXinerama-devel libXrandr-devel libXScrnSaver-devel
    # haskell
    gcc gcc-c++ gmp gmp-devel make ncurses ncurses-compat-libs xz perl
    # xmobar
    xmobar
    # errors for xmonad
    xmessage
    # keyring
    gnome-keyring
)
for p in "${programs[@]}"; do
    yn "$p?" && selected+=("$p") || unselected+=("$p");
done
sudo dnf install --allowerasing -y "${selected[@]}"

# zoxide config
if command -v zoxide &> /dev/null
then
    throw_bashrc "eval \"\$(zoxide init bash)\""
fi

# tmux config
if command -v tmux &> /dev/null
then
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi

# firewall
if command -v ufw &> /dev/null
then
    sudo ufw enable
    sudo systemctl enable ufw
    sudo ufw limit 22/tcp
    sudo ufw allow 80/tcp
    sudo ufw allow 443/tcp
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
fi

# XMonad
function xmonad {
    git clone https://gitlab.com/Uangn/my-xmonad.git ~/.config/xmonad
        # GHCup
    curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
    source "$HOME/.ghcup/env"
        # sub repos
    cd ~/.config/xmonad
    git clone --depth=1 --branch v0.18.0 https://github.com/xmonad/xmonad
    git clone --depth=1 --branch v0.18.0 https://github.com/xmonad/xmonad-contrib
        # compile
    stack install
}
yn "XMonad?" && xmonad

function sddm {
    sudo systemctl enable sddm.service
    sudo systemctl set-default graphical.target
    sudo cp -r ~/.config/sddm-themes/my-sddm-catppuccin-mocha /usr/share/sddm/themes/catppuccin-mocha
        # add to launchable
    sudo sh -c 'printf "[Desktop Entry]\nEncoding=UTF-8\nName=xmonad\nComment=This starts xmonad\nExec=~/.local/bin/xmonad\nType=Application" > /usr/share/xsessions/xmonad.desktop'
    sudo sh -c 'printf "[General]\nDisplayServer=x11" > /etc/sddm.conf.d/x11.conf'
    sudo sh -c 'printf "[Theme]\nCurrent=catppuccin-mocha" > /etc/sddm.conf.d/theme.conf'
}
yn "sddm?" && sddm

# console fonts
function consolefonts {
    sudo mkdir -p "/etc/default/"
    sudo touch "/etc/default/console-setup"
    if [ -z $(grep "FONT" "/etc/default/console-setup") ]; then
        sudo sh -c "echo 'FONT=\"ter-v32b.psf.gz\"' >> \"/etc/default/console-setup\""
    else
         sudo sed -i 's/FONT=.*/FONT="ter-v32b\.psf\.gz"/' "/etc/default/console-setup"
    fi
}
yn "Console Fonts?" && consolefonts

# Get Fonts
function firacodefonts {
    mkdir -p ~/Themeing/Fonts/FiraCode
    wget --output-document ~/Themeing/Fonts/FiraCode.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/FiraCode.zip
    7za x "$HOME/Themeing/Fonts/FiraCode.zip" -o"$HOME/Themeing/Fonts/FiraCode"

    sudo mkdir -p /usr/local/share/fonts/FiraCode
    sudo cp "$HOME/Themeing/Fonts/FiraCode/* /usr/local/share/fonts/FiraCode/"

    sudo chown -R root: /usr/local/share/fonts/FiraCode
    sudo chmod 644 /usr/local/share/fonts/FiraCode/*
    sudo restorecon -vFr /usr/local/share/fonts/FiraCode
    sudo fc-cache -v
}
yn "Fira Code Fonts?" && firacodefonts

function removerhgb {
    sudo sed -i 's/ rhgb quiet//' /etc/default/grub
    sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
}
yn "Remove RHGB?" && removerhgb

# Rust Tools
function rusttools {
    # YEA I KNOW I CANT JUST CHECK CARGO, SHUT
    if [ command -v cargo &> /dev/null ]; then
        echo "cargo already installed"
    else
        curl https://sh.rustup.rs -sSf | sh
        . "$HOME/.cargo/env"
    fi
}

# Starship
function starship {
    rusttools
    cargo install starship --locked
    throw_bashrc "eval \"\$(starship init bash)\""
}
yn "Starship?" && starship

# Silicon
function silicon {
    sudo dnf install -y \
        cmake \
        expat-devel fontconfig-devel libxcb-devel \
        freetype-devel libxml2-devel \
        harfbuzz
    rusttools
    cargo install silicon --locked
}
yn "Silicon?" && silicon

# OpenTabletDriver
function otd {
    mkdir -p ~/Drivers
    wget --output-document ~/Drivers/OpenTabletDriver.rpm https://github.com/OpenTabletDriver/OpenTabletDriver/releases/latest/download/OpenTabletDriver.rpm
    sudo dnf install -y ~/Drivers/OpenTabletDriver.rpm
    systemctl --user enable opentabletdriver.service --now
}
yn "OpenTabletDriver?" && otd


# virtualization
function virt {
    if [ $(getent group libvirt) ]; then
        sudo useradd -g $USER libvirt
        sudo useradd -g $USER libvirt-kvm
        sudo usermod -aG libvirt $USER
    fi
    sudo systemctl enable libvirtd.service && sudo systemctl start libvirtd.service
}
yn "Virtualization?" && virt

# flathub
function flathub {
    flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
}
yn "Flathub?" && flathub

# browser
function floorp {
    flatpak install -y flathub one.ablaze.floorp
    if [ ! -f "/usr/bin/Floorp" ]; then
        sudo ln -s /var/lib/flatpak/exports/bin/one.ablaze.floorp /usr/bin/floorp
        sudo ln -s /var/lib/flatpak/exports/bin/one.ablaze.floorp /usr/bin/floorp-private
        sudo sed -i 's/"/--private-window "/' /usr/bin/floorp-private
    fi
}
yn "Floorp?" && floorp

# Discord (vencord)
function vencord {
    sudo dnf install -y discord
    # we nukeing ourselves with this one boiz
    sh -c "$(curl -sS https://raw.githubusercontent.com/Vendicated/VencordInstaller/main/install.sh)"
}
yn "vencord?" && vencord

# Discord (Vesktop)
function vesktop {
    flatpak install -y flathub dev.vencord.Vesktop
    sudo ln -s /var/lib/flatpak/exports/bin/dev.vencord.Vesktop /usr/bin/vesktop
}
yn "Vesktop?" && vesktop

# Doc editor
function onlyoffice {
    flatpak install -y flathub org.onlyoffice.desktopeditors
    if [ ! -f "/usr/bin/onlyoffice" ]; then
        sudo ln -s /var/lib/flatpak/exports/bin/org.onlyoffice.desktopeditors /usr/bin/onlyoffice
    fi
}
yn "Onlyoffice?" && onlyoffice

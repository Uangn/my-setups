#!/bin/bash

sudo zypper install git make gcc gcc-c++ gmp-devel libX11-devel libXrandr-devel libXss-devel libXft-devel xorg-x11

# brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo >> $HOME/.bashrc
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> $HOME/.bashrc
source $HOME/.bashrc

# asdf
brew install asdf
echo >> $HOME/.bashrc
echo 'export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"' >> $HOME/.bashrc
source $HOME/.bashrc

# haskell
asdf plugin add ghc
asdf plugin add stack
asdf plugin add cabal
asdf install stack latest
asdf set -u stack latest

# xmonad
mkdir $HOME/.config/xmonad
cd $HOME/.config/xmonad
git clone https://github.com/xmonad/xmonad.git
git clone https://github.com/xmonad/xmonad-contrib.git
# add config to xmonad.hs
stack init
stack build --verbose
stack install

# all
sudo zypper install \
    git ssh ripgrep zoxide fd bat neovim starship tmux tealdeer alacritty\
    fira-code-fonts fontawesome-fonts google-noto-fonts google-noto-coloremoji-fonts\
    xev btop dunst nnn unzip unrar 7zip pcmanfm\
    lutris wine steam\
    xmobar xorg-x11 xorg-x11-server xterm x11-tools xclip xset i3lock rofi brightnessctl\
    easyeffects pavucontrol pulseaudio-utils alsa-utils yt-dlp\
    MozillaFirefox discord\
    screenkey flameshot\
    ffmpeg-7 ImageMagick krita\
    mpv cmus feh zathura zathura-plugin-pdf-mupdf\
    gparted ncdu udiskie\
    piper xmodmap

# clipit
# https://software.opensuse.org//download.html?project=home%3AAndnoVember%3ALXQt&package=clipit
sudo zypper addrepo https://download.opensuse.org/repositories/home:AndnoVember:LXQt/openSUSE_Tumbleweed/home:AndnoVember:LXQt.repo
sudo zypper refresh
sudo zypper install clipit

# remove
sudo zypper remove plymouth

# tty font
sudo zypper install terminus-bitmap-fonts
sudo sed -i /etc/vconsole.conf -e "s/FONT=\(\w*\)/FONT=ter-v32n.psfu/"

# virtualization
sudo zypper install -t pattern kvm_server kvm_tools
sudo zypper install libvirt-daemon
sudo usermod --append --groups libvirt "$USER"
sudo systemctl enable --now libvirtd

# silicon
asdf plugin add rust
asdf install rust latest
asdf set -u rust latest
cargo install silicon
asdf reshim

# tmux tpm
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# other
sudo hostnamectl set-hostname "$USER"

mkdir ~/.vim

xdg-mime default pcmanfm.desktop inode/directory
xdg-mime default pcmanfm.desktop application/octet-stream
